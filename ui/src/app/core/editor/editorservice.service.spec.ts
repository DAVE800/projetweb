import { TestBed } from '@angular/core/testing';

import { EditorserviceService } from './editorservice.service';

describe('EditorserviceService', () => {
  let service: EditorserviceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EditorserviceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
